var assert = require('assert');
var login = require('../../src/login')

describe('Test Login', function () {
    var testValidUser = {
        username: 'validuser',
        password: 'strongpassword'
    }

    var testInvalidUser = {
        username: 'invaliduser',
        password: 'wrongpassword'
    }

    it('[驗證密碼] 合法使用者，須回傳驗證成功', function(done) {
        var result = login.checkPsw(testValidUser.password);
        assert.deepEqual(result, true);
        done();
    });

    it('[驗證密碼] 非法使用者，須回傳驗證失敗', function(done) {
        var result = login.checkPsw(testInvalidUser.password);
        assert.deepEqual(result, false);
        done();
    });

    it('[驗證歡迎訊息] 須回傳正確訊息', function(done) {
        var msg = login.getHelloMsg(testValidUser.username);
        assert.deepEqual(msg, 'Hello, validuser! Welcome to our website.');
        done();
    });
})